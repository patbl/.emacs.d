(setq gc-cons-threshold (eval-when-compile (* 1024 1024 1024)))
(run-with-idle-timer 2 t (lambda () (garbage-collect)))

(require 'server)
(unless (server-running-p)
  (server-start))

(if (eq system-type 'darwin)
    (setq default-directory "/Users/pat"))

(package-initialize)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))
(setq package-enable-at-startup nil)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (message "lol")
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(setq use-package-always-ensure t)
(setq use-package-verbose t)

(when (memq window-system '(mac ns))
  (use-package
    exec-path-from-shell
    :config
    (exec-path-from-shell-initialize)))

(defun my-kill-this-buffer ()
  (interactive)
  (let (kill-buffer-query-functions) (kill-buffer)))

(use-package yasnippet
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets)

(use-package smartparens-config
    :ensure smartparens
    :config
    (show-smartparens-global-mode t)
    (add-hook 'emacs-lisp-mode-hook (lambda() (smartparens-mode)))
    (setq smartparens-strict-mode t)
    (setq sp-cancel-autoskip-on-backward-movement nil)
    (setq sp-autoskip-closing-pair "always")
    (sp-use-smartparens-bindings)
    (define-key smartparens-mode-map (kbd "M-<backspace>") nil)
    (sp-pair "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
    (sp-local-pair 'ruby-mode "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
    (sp-local-pair 'ruby-mode "[" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
    (defun my-create-newline-and-enter-sexp (&rest _ignored)
      "Open a new brace or bracket expression, with relevant newlines and indent. "
      (newline)
      (indent-according-to-mode)
      (forward-line -1)
      (indent-according-to-mode)))

(use-package key-chord
  :config
  (setq key-chord-two-keys-delay .025)
  (add-hook 'minibuffer-setup-hook
            (lambda () (set (make-local-variable 'input-method-function) nil))))

(use-package use-package-chords
  :config (key-chord-mode 1))

(use-package unbound
  :load-path "vendor")

(use-package volatile-highlights
  :config (volatile-highlights-mode t))

(use-package avy
  :chords (("vc" . avy-goto-char-2)
           ("cx" . avy-goto-word-1))
  :config
  (setq avy-single-candidate-jump nil)
  (setq avy-dispatch-alist '((?x . avy-action-kill-move)
                             (?X . avy-action-kill-stay)
                             (?p . avy-action-teleport) ; changed to avoid conflict
                             (?m . avy-action-mark)
                             (?c . avy-action-copy) ; changed to avoid conflict
                             (?y . avy-action-yank)
                             (?i . avy-action-ispell)
                             (?z . avy-action-zap-to-char)))
  (setq avy-keys '(?a ?r ?s ?t ?d ?h ?n ?e ?i ?o)))

(use-package counsel-projectile
  :config
  (counsel-projectile-mode)
  (setq counsel-projectile-rg-initial-input
        '(projectile-symbol-or-selection-at-point)))

(use-package projectile
  :config
  (projectile-mode)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (setq projectile-ignored-projects (list "~/academia/projects/academia-app2/" "~/academia/projects/academia-app3/" "~/academia/projects/academia-zoo/"))
  (setq projectile-enable-caching t)
  (setq projectile-create-missing-test-files t)
  (setq projectile-switch-project-action #'counsel-projectile-find-file))

(use-package web-mode
  :config
  (setq web-mode-enable-auto-quoting nil)
  (setq web-mode-enable-auto-indentation nil))

;; https://emacs.stackexchange.com/a/47415/2679
(add-hook 'web-mode-hook
          (lambda ()
            ;; short circuit js mode and just do everything in jsx-mode
            (if (equal web-mode-content-type "javascript")
                (web-mode-set-content-type "jsx")
              (message "now set to: %s" web-mode-content-type))))

(mapc (lambda (element)
        (add-to-list 'auto-mode-alist element))
      (list
       '(".pryrc\\'" . ruby-mode)
       '("\\zshrc\\'" . shell-script-mode)
       '("\\zshenv\\'" . shell-script-mode)
       '("\\zprofile\\'" . shell-script-mode)
       '("\\zpath\\'" . shell-script-mode)
       '("\\bashrc\\'" . shell-script-mode)
       '("\\zsh-theme\\'" . shell-script-mode)
       '("\\.erb\\'" . web-mode)
       '("\\.html?\\'" . web-mode)
       '("\\.inky\\'" . web-mode)
       '("\\.j2\\'" . web-mode)
       '("\\.inky-slim\\'" . slim-mode)
       '("\\.jsx\\'" . web-mode)
       '("\\.js\\'" . web-mode)
       '("\\.es6\\'" . web-mode)
       '("\\.js.jsx.coffee\\'" . coffee-mode)
       '("\\.jade\\'" . pug-mode)
       '("\\.json\\'" . json-mode)))

(use-package flycheck
  :init
  (global-flycheck-mode)
  :config
  (setq-default flycheck-disabled-checkers
                '(sh-posix-dash emacs-lisp-checkdoc))
  (custom-set-faces
   '(flycheck-error ((((class color)) (:underline (:style wave :color "Red")))))
   '(flycheck-warning ((((class color)) (:underline "Orange")))))
  (setq flycheck-indication-mode nil)
  (flycheck-add-mode 'javascript-eslint 'js2-mode)
  (flycheck-add-mode 'javascript-eslint 'web-mode))

(use-package evil
  :config
  (global-set-key (kbd "C-l") 'evil-ex-nohighlight)
  (setq evil-want-fine-undo t)
  (setq evil-ex-substitute-global t)
  (setq evil-ex-search-vim-style-regexp t)
  (evil-select-search-module 'evil-search-module 'evil-search)
  (setq evil-want-visual-char-semi-exclusive t)
  (setq evil-ex-search-persistent-highlight nil)
  (setq-default evil-shift-width 2)
  (setq-default evil-symbol-word-search t)
  (setq evil-emacs-state-cursor '("red" box))
  (setq evil-normal-state-cursor '("purple" box))
  (setq evil-visual-state-cursor '("orange" box))
  (setq evil-insert-state-cursor '("red" bar))
  (setq evil-replace-state-cursor '("red" bar))
  (setq evil-operator-state-cursor '("red" hollow))

  ;; Make movement keys work like they should
  (define-key evil-normal-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
  (define-key evil-normal-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
  (define-key evil-motion-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
  (define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)

  (evil-mode 1)
  (setq evil-cross-lines 1))

(use-package evil-surround
  :config (global-evil-surround-mode 1))

(use-package evil-magit)

(define-key evil-normal-state-map (kbd "RET") 'newline-and-indent)
(define-key evil-normal-state-map (kbd "<backspace>") 'backward-delete-char-untabify)

(key-chord-define-global "ii" 'evil-normal-state)
(key-chord-define-global "km" '(lambda ()
           (interactive)
           (newline-and-indent)
           (previous-line)
           (end-of-line)
           (newline-and-indent)))
(key-chord-define-global "hn" 'evil-open-above)

(use-package evil-nerd-commenter)
(use-package evil-leader
  :config
  (evil-leader/set-leader "<SPC>")
  (global-evil-leader-mode)
  (evil-leader/set-key
    "o" 'find-file
    "p" 'counsel-projectile-switch-project
    "v" 'patbl-verbose-whitespace
    "V" 'whitespace-mode
    "w" 'my-kill-this-buffer
    "s" 'save-buffer
    "d" 'dired
    "r" 'evil-window-next
    "ci" 'evilnc-comment-or-uncomment-lines
    "cl" 'evilnc-quick-comment-or-uncomment-to-the-line
    "cc" 'evilnc-copy-and-comment-lines
    "cp" 'evilnc-comment-or-uncomment-paragraphs
    "cr" 'comment-or-uncomment-region
    "cv" 'evilnc-toggle-invert-comment-line-by-line
    "/"  'evilnc-comment-operator
    "."  'evilnc-copy-and-comment-operator
    "no" 'dumb-jump-go-other-window
    "nj" 'dumb-jump-go
    "nx" 'dumb-jump-go-prefer-external
    "nz" 'dumb-jump-go-prefer-external-other-window
    "nb" 'dumb-jump-back
    "nq" 'dumb-jump-quick-look
    "np" 'dumb-jump-go-prompt
    ))

;;; reload current file
(defun reload-current-file ()
  "Reload current file."
  (interactive)
  (save-buffer (current-buffer))
  (load-file (buffer-file-name)))
(global-set-key (kbd "<C-f5>") 'reload-current-file)

;;; makes shebang
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

(defun switch-between-buffers ()
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(global-set-key (kbd "M-o") 'switch-between-buffers)

(defun new-empty-buffer ()
  "Opens a new empty buffer."
  (interactive)
  (let ((buf (generate-new-buffer "untitled")))
    (switch-to-buffer buf)
    (funcall (and initial-major-mode))
    (setq buffer-offer-save t)))
(global-set-key (kbd "C-c n") 'new-empty-buffer)

(global-set-key (kbd "C-x y") 'yas-visit-snippet-file)
(global-set-key (kbd "C-x x") 'yas-reload-all)

(global-set-key (kbd "C-c i") '(lambda() (interactive) (find-file-existing "~/.emacs.d/init.el")))

(global-display-line-numbers-mode t)
(global-set-key (kbd "M-s l") 'display-line-numbers-mode)
(global-visual-line-mode t)
(setq-default word-wrap t)
(setq ring-bell-function 'ignore) ; turn off annoying alarm-bell flashing
(blink-cursor-mode (- (*) (*) (*))) ; stop Chinese water-torture blinking cursor
(global-hl-line-mode 1) ; highlight current line

;;; don't ask for confirmation when creating a new file or buffer
(setq confirm-nonexistent-file-or-buffer nil)

(fset 'yes-or-no-p 'y-or-n-p)

;;; turn off welcome page
(setq inhibit-startup-message t)

;;; automatically reload files updated by other programs
(global-auto-revert-mode t)
;; turn off auto revert messages
(setq auto-revert-verbose nil)

;;; keep custom settings from cluttering up .emacs
(setq custom-file "~/.emacs-custom")
(load custom-file 'noerror)

;; I hate tabs!
(setq-default indent-tabs-mode nil)

;; Put autosave files (ie #foo#) and backup files (ie foo~) in ~/.emacs.d/.
(setq auto-save-file-name-transforms `((".*" "~/.emacs.d/.autosaves" t)))
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))
(setq backup-by-copying t)

;; show the coördinates of the cursor
(column-number-mode 1)

;; keep cursor in same relative location when using page up/down
(setq scroll-preserve-screen-position t)

(setq vc-follow-symlinks t)

;; allow blank files to be saved
(defun assume-new-is-modified ()
  (when (not (file-exists-p (buffer-file-name)))
    (set-buffer-modified-p t)))
(add-hook 'find-file-hooks 'assume-new-is-modified)

;; enable aliases when running shell-command
(setq shell-file-name "zsh")
(setq shell-command-switch "-ic")

;;;; Keybindings

;; Move to previous/next sentence
(setq sentence-end-double-space nil)

(global-set-key (kbd "C-S-n") 'make-frame)
(global-set-key (kbd "C-c w q") 'kill-emacs)
(global-set-key (kbd "C-S-q") 'delete-frame)

(global-set-key (kbd "M-A") 'shell-command)
(global-set-key (kbd "M-k") 'my-kill-this-buffer)
(global-set-key (kbd "C-s") 'save-buffer)

(delete-selection-mode t)

(defun delete-trailing-whitespace-except-current-line ()
  (interactive)
  (let ((begin (line-beginning-position))
        (end (line-end-position)))
    (save-excursion
      (when (< (point-min) begin)
        (save-restriction
          (narrow-to-region (point-min) (1- begin))
          (delete-trailing-whitespace)))
      (when (> (point-max) end)
        (save-restriction
          (narrow-to-region (1+ end) (point-max))
          (delete-trailing-whitespace))))))
(add-hook 'before-save-hook 'delete-trailing-whitespace-except-current-line)

;; smarter forward/backward/kill word with camel case
(global-subword-mode t)

;; for better jsx syntax-highlighting in web-mode
(defadvice web-mode-highlight-part (around tweak-jsx activate)
  (if (equal web-mode-content-type "jsx")
    (let ((web-mode-enable-part-face nil))
      ad-do-it)
    ad-do-it))

;; insert closing tag after closing opening tag with ">"
(setq web-mode-tag-auto-close-style 2)
(setq web-mode-attr-indent-offset 2)

;; allow web-mode to close tags
(defun make-sp-and-web-mode-play-nice ()
  (sp-with-modes sp--html-modes
    (sp-local-pair "<" nil :actions nil)))
(add-hook 'web-mode-hook 'make-sp-and-web-mode-play-nice)

(add-hook 'sql-mode-hook 'sql-highlight-postgres-keywords)

(use-package swap-regions
  :bind ("C-x C-t" . swap-regions))

(global-set-key (kbd "C-c f a")
                '(lambda ()
                   (interactive)
                   (kill-new (buffer-file-name))
                   (message (buffer-file-name))))
(global-set-key (kbd "C-c f n") '(lambda () (interactive) (kill-new (buffer-name))))
(global-set-key (kbd "C-c f r")
                '(lambda () (interactive)
                   (kill-new
                    (file-relative-name (buffer-file-name) (vc-root-dir)))))

(defun patbl-insert-debugger ()
  (interactive)
  (save-excursion
    (let ((debugging-command
           (cond ((string= major-mode "ruby-mode") "binding.pry")
                 ((string= major-mode "enh-ruby-mode") "binding.pry")
                 ((string= major-mode "web-mode") "debugger")
                 ((string= major-mode "python-mode") "import pdb; pdb.set_trace()")
                 ((string= major-mode "js2-mode") "debugger"))))
      (when debugging-command
          (beginning-of-line)
        (newline-and-indent)
        (forward-line -1)
        (indent-according-to-mode)
        (insert debugging-command)
        (save-buffer)))))
(key-chord-define-global "bk" 'patbl-insert-debugger)

(use-package wgrep
  :config
  (setq wgrep-auto-save-buffer t)
  (setq wgrep-change-readonly-file t))

(setq require-final-newline t)

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-code-indent-offset 2))

(add-hook 'web-mode-hook 'my-web-mode-hook)

(use-package js2-mode
  :config
  (setq js2-basic-offset 2)
  (setq js2-strict-trailing-comma-warning nil)
  (setq js2-strict-missing-semi-warning nil)
  (add-hook 'js2-mode-hook (lambda () (setq js2-basic-offset 2))))

(use-package magit
  :chords (("uy" . magit-status))
  :bind (("C-c C-c" . magit-commit)
         ("C-c e" . magit-blame-addition)
         ("C-c l" . magit-log-buffer-file)
         ("C-c v" . magit-file-dispatch)
         ("C-c C-v" . magit-dispatch)
         :map magit-mode-map
         ("C-7" . magit-section-show-level-1)
         ("C-8" . magit-section-show-level-2)
         ("C-9" . magit-section-show-level-3)
         ("C-0" . magit-section-show-level-4)
         ("C-n" . next-line))
  :config
  (add-hook 'git-commit-mode-hook (lambda () (setq fill-column 72)))
  (setq git-commit-summary-max-length 72)
  (add-hook 'with-editor-mode-hook 'evil-insert-state)
  (setq magit-save-repository-buffers "dontask")
  (global-auto-revert-mode t)
  (global-magit-file-mode t)
  (setq magit-auto-revert-immediately t))

(defun my-delete-this-buffer ()
  (interactive)
  (shell-command (concat "rm " (buffer-file-name)))
  (my-kill-this-buffer))
(global-set-key (kbd "C-c M-k") 'my-delete-this-buffer)

(winner-mode 1)

(setq imenu-auto-rescan t)
(key-chord-define-global ",." 'counsel-imenu)

(setq-default tab-width 2)

(setq large-file-warning-threshold 100000000)
(setq text-quoting-style 'grave)

(use-package whitespace-cleanup-mode
  :config
  (global-whitespace-cleanup-mode))

(use-package anzu
  :config
  (global-anzu-mode +1))

(use-package evil-anzu
  :no-require t)

(with-eval-after-load 'evil
  (require 'evil-anzu))

(use-package clipmon
  :config
  (add-to-list 'after-init-hook 'clipmon-mode-start))

(use-package ace-window
  :config
  (set-face-attribute 'aw-leading-char-face nil :height 400)
  :chords (("wf" . ace-window)))

(use-package counsel
  :chords (("lu" . counsel-recentf))
  :config
  (setq counsel-rg-base-command "rg -i -M 240 --no-heading --sort-files --smart-case --line-number --color never %s .")
  (setq counsel-grep-base-command "rg -i -M 240 --no-heading --sort-files --smart-case --line-number --color never '%s' %s")
  (setq counsel-find-file-occur-cmd "ls -a | grep -i -E '%s' | tr '\\n' '\\0' | xargs -0 ls -d")
  (ivy-set-occur 'ivy-switch-buffer 'ivy-switch-buffer-occur)
  :bind
  ("M-x" . counsel-M-x)
  ("M-a" . counsel-projectile-rg)
  ("M-f" . counsel-projectile-find-file)
  ("C-c s" . swiper)
  ("C-c C-s" . swiper-isearch)
  ("C-c a" . swiper-all)
  ("C-c u" . counsel-unicode-char)
  ("C-h v" . counsel-describe-variable)
  ("C-h f" . counsel-describe-function)
  ("C-c b" . counsel-bookmark))

(use-package ivy
  :diminish (ivy-mode . "")
  :bind
  (("C-c C-r" . ivy-resume)
   :map ivy-minibuffer-map
   ("<return>" . ivy-alt-done)
   ("C-c l" . ivy-rotate-preferred-builders))
  :chords (("fp" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-count-format "%d/%d ")
  (setq ivy-extra-directories '())
  (setq ivy-flx-limit 10000)
  ;; add ‘recentf-mode’ and bookmarks to ‘ivy-switch-buffer’.
  (setq ivy-use-virtual-buffers t)
  (recentf-mode 1)
  ;; number of result lines to display
  (setq ivy-height 20)
  ;; no regexp by default
  (setq ivy-initial-inputs-alist nil)
  ;; configure regexp engine.
  (setq ivy-re-builders-alist
        '((read-file-name-internal . ivy--regex-fuzzy)
          (counsel-projectile-find-file . ivy--regex-fuzzy)
          (ivy-switch-buffer . ivy--regex-fuzzy)
          (counsel-ag . ivy--regex-ignore-order)
          (counsel-recentf . ivy--regex-fuzzy)
          (swiper . ivy--regex-plus)
          (t . ivy--regex-plus)))
  )

(use-package swiper)

(use-package company
  :bind (("M-/" . company-complete))
  :init
  (setq company-global-modes '(not org-mode text-mode))
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-idle-delay 0.5)
  :config
  (setq company-frontends
        '(company-tng-frontend
          company-pseudo-tooltip-unless-just-one-frontend
          company-echo-metadata-frontend)))

(use-package expand-region
  :bind (("C->" . er/expand-region)
         ("C-<" . er/contract-region)))

(use-package git-link
  :bind
  (("C-c g" . git-link))
  (("C-c C-g" . git-link-commit))
  :config
  (setq git-link-open-in-browser t))

(setq sh-basic-offset 2)
(setq sh-indentation 2)

(use-package dumb-jump
  :config
  (setq dumb-jump-selector 'ivy)
  (setq dumb-jump-quiet t))

(require 'dired-x)

(setq recentf-max-saved-items 500)

(save-place-mode 1)

(use-package ibuffer-vc
  :config
  (add-hook 'ibuffer-hook
            (lambda ()
              (ibuffer-vc-set-filter-groups-by-vc-root)
              (unless (eq ibuffer-sorting-mode 'alphabetic)
                (ibuffer-do-sort-by-alphabetic)))))

(defadvice sh--maybe-here-document (around be-smart-about-it activate)
  "Do normal here doc auto insert, but if you type another chevron, revert and leave just <<<."
  (if (and (= (current-column) 1)
           (looking-back "^<" 0)
           (looking-at "\nEOF")
           (save-excursion
             (forward-line -1)
             (end-of-line 1)
             (looking-back "<<EOF" 0)))
      (progn (delete-region (search-backward "EOF") (search-forward "EOF" nil t 2))
             (insert "<"))
    ad-do-it))

(setq-default fill-column 80)

(use-package smart-mode-line
  :config
  (sml/setup))

(use-package rg)

(add-hook
 'web-mode-hook
 (lambda ()
   (let ((current-buffer-name (buffer-file-name (current-buffer))))
     (progn
       (when (and
              (string-suffix-p ".js.jsx" current-buffer-name)
              (string-match-p (regexp-quote "academia-app") current-buffer-name))
         (setq flycheck-disabled-checkers '(javascript-eslint)))
       (when (string-suffix-p ".erb" current-buffer-name)
         (setq flycheck-disabled-checkers '(javascript-eslint)))
       ))))

(use-package coffee-mode
  :config
  (custom-set-variables '(coffee-tab-width 2)))

(use-package bookmark
  :defer t
  :config
  (progn
    (defun bookmark-find-from-dir-or-default (orig-fun bmk-record)
      "Around Advice for bookmark-default-handler.  Calls
             through unless bookmark is a directory, in which
             case, calls counsel-find-file."
      (let ((file (bookmark-get-filename bmk-record)))
        (if (file-directory-p file)
            (let ((default-directory file))
              (call-interactively 'find-file))
          (funcall orig-fun bmk-record))))
    (advice-add `bookmark-default-handler
                :around #'bookmark-find-from-dir-or-default)))

(use-package terraform-mode)

(setq initial-major-mode 'fundamental-mode)

(use-package enh-ruby-mode
  :config
  (setq enh-ruby-deep-indent-construct nil)
  (add-to-list 'auto-mode-alist
               '("\\(?:\\.rb\\|ru\\|rake\\|thor\\|jbuilder\\|gemspec\\|podspec\\|/\\(?:Gem\\|Rake\\|Cap\\|Thor\\|Vagrant\\|Guard\\|Pod\\)file\\)\\'\\|Gemfile-railsnext\\'" . enh-ruby-mode)))

(setenv "ON_HOST_MACHINE" "true")

(defun my-frame-tweaks (&optional frame)
  "My personal frame tweaks."
  (unless frame
    (setq frame (selected-frame)))
  (when frame
    (with-selected-frame frame
      (when (display-graphic-p)
        (scroll-bar-mode 0)
        (menu-bar-mode 0)
        (tool-bar-mode 0)))))
;; For the case that the init file runs after the frame has been created.
;; Call of emacs without --daemon option.
(my-frame-tweaks)
;; For the case that the init file runs before the frame is created.
;; Call of emacs with --daemon option.
(add-hook 'after-make-frame-functions #'my-frame-tweaks t)

(use-package pug-mode)

(use-package rbenv
 :config
 (global-rbenv-mode))

(global-set-key (kbd "s-u") 'revert-buffer)

(setq x-select-enable-clipboard t)

(defun my/use-eslint-from-node-modules ()
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/.bin/eslint"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))
(add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)

(setq whitespace-line-column 100)
(setq whitespace-style '(face lines-tail))
(global-whitespace-mode 1)
(defun patbl-verbose-whitespace()
  (interactive)
  (setq-local whitespace-style (eval (car (get 'whitespace-style 'standard-value))))
  (whitespace-mode 1))

(use-package which-key
  :config
  (which-key-mode))

(use-package bind-chord)
(use-package bind-key)
(use-package dired-single)
(use-package dockerfile-mode)
(use-package jinja2-mode)
(use-package json-mode)
(use-package moonscript)
(use-package nginx-mode)
(use-package ruby-hash-syntax
  :bind
  (:map enh-ruby-mode-map
        ("C-c h" . 'ruby-hash-syntax-toggle)))
(use-package scss-mode)
(use-package slim-mode)
(use-package yaml-mode)
(use-package rspec-mode)
(use-package smex)
(use-package flx)

(pixel-scroll-mode)

(defun arrayify (start end quote)
  "Turn strings on newlines into a QUOTEd, comma-separated one-liner."
  (interactive "r\nMQuote: ")
  (let ((insertion
         (mapconcat
          (lambda (x) (format "%s%s%s" quote x quote))
          (split-string (buffer-substring start end)) ", ")))
    (delete-region start end)
    (insert insertion)))

(use-package solarized-theme
  :config
  (load-theme 'solarized-light t))

(use-package evil-numbers
  :config
  (global-set-key (kbd "C-c +") 'evil-numbers/inc-at-pt)
  (global-set-key (kbd "C-c -") 'evil-numbers/dec-at-pt))

(set-frame-font "Iosevka 14" nil t)

(with-eval-after-load 'evil
    (defalias #'forward-evil-word #'forward-evil-symbol)
    ;; make evil-search-word look for symbol rather than word boundaries
    (setq-default evil-symbol-word-search t))

(use-package default-text-scale
  :config
  (default-text-scale-mode))

(use-package term-cursor
  :load-path "vendor"
  :config
  (global-term-cursor-mode))

(put 'dired-find-alternate-file 'disabled nil)
(add-hook 'dired-mode-hook
          (lambda ()
            (define-key dired-mode-map (kbd "^")
              (lambda () (interactive) (find-alternate-file "..")))))
